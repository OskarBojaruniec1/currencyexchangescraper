package mails;

import website.TableToFile;

import javax.mail.*;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Properties;

public class SendMail {

    public void Send(String mail) throws IOException, MessagingException {

        MailProperties mp = new MailProperties();
        Properties prop = mp.getProperties();

        MailSession ms = new MailSession();
        Session session = ms.getSession(prop);

        TableToFile table = new TableToFile();
        table.getSiteToFile();

        Message message = new MimeMessage(session);

        message.setFrom(new InternetAddress("oskar.bojaruniec2@gmail.com"));
        message.setRecipients(
                Message.RecipientType.TO, InternetAddress.parse(mail));
        message.setSubject("CurrencyRates");

        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
        attachmentBodyPart.attachFile("currency-rates.csv");

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(attachmentBodyPart);

        message.setContent(multipart);

        Transport.send(message);

    }
}
