package mails;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

public class MailSession {

    public Session getSession(Properties prop) {

        return javax.mail.Session.getInstance(prop, new Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("login", "password");
            }
        });
    }
}
