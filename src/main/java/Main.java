import mails.SendMail;

import javax.mail.MessagingException;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, MessagingException {

        SendMail sm = new SendMail();
        sm.Send("example-email@gmail.com");

    }
}
