package website;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class AccessToWebsite {

    private Document websiteDocument;

    public AccessToWebsite(String url) {

        try {
            this.websiteDocument = Jsoup.connect(url).get();
        } catch (IOException e) {
            System.out.println("Failed connect to website");
        }
    }

    public Document getWebsiteDocument() {
        return websiteDocument;
    }

}
