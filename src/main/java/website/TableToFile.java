package website;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class TableToFile {

    public void getSiteToFile() throws IOException {

        TableOfCurrencyRate table = new TableOfCurrencyRate();
        File tableToFile = new File("currency-rates.csv");

        FileOutputStream fos = new FileOutputStream(tableToFile);
        OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        BufferedWriter writer = new BufferedWriter(osw);

        for (String line : table.getTableOfCurrencyRate()) {

            writer.write(line);
            writer.newLine();
        }

        writer.flush();
        writer.close();

        tableToFile.deleteOnExit();

    }
}