package website;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class TableOfCurrencyRate {

    public List<String> getTableOfCurrencyRate() {

        AccessToWebsite website = new AccessToWebsite("https://cinkciarz.pl/wymiana-walut/kursy-walut");
        Document websiteDoc = website.getWebsiteDocument();

        Element table = websiteDoc.getElementById("currencies-rates");
        assert table != null : "table is empty";

        Elements rows = table.select("tr");
        List<String> rowsList = new ArrayList<>();

        for (Element row : rows) {

            String name = row.getElementsByAttributeValue("data-label", "Nazwa").text();
            if (name.isEmpty()) continue;
            String code = row.getElementsByAttributeValue("data-label", "Kod waluty").text();
            String buy = row.getElementsByAttributeValue("data-buy", "true").text();
            String sell = row.getElementsByAttributeValue("data-sell", "true").text();

            String line = name + "," +
                    code + "," +
                    buy.replace(",", ".") + "," +
                    sell.replace(",", ".");

            rowsList.add(line);

        }

        return rowsList;
    }
}
